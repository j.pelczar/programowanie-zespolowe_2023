# Zajęcia:
Planowane tematy


## Co to SCRUM/AGILE/KANBAN?
 - Waterfal vs Agile
 - Sprinty
 - Tabela zadań
 - dokumentacja i rozpisywanie zadan, aby każdy zrozumiał o co chodzi w zadaniu

 https://scrumguides.org/docs/scrumguide/v2020/2020-Scrum-Guide-Polish.pdf
 
 https://kanbanize.com/kanban-resources/getting-started/what-is-kanban


## Co to git?

https://www.freecodecamp.org/news/git-internals-objects-branches-create-repo/

https://education.github.com/git-cheat-sheet-education.pdf

https://www.atlassian.com/continuous-delivery/continuous-integration/trunk-based-development

## Co to jest Gitlab?
 - zapoznanie się, założenie kont, tworzenie grup
 - dla chętnych - self host
 - all in one solution

## Co to jest CI/CD:
 - .gitlab-ci.yml
 - testowanie 
 - docker
 - trzymanie hasel
 - testowanie
 - swoje runnery - na wydziale jest maszyna wirtualna, dla odpalania testów/buildów runnerem gitlabowym
 - https://docs.gitlab.com/ee/ci/
 - https://docs.gitlab.com/runner/

 ## Odtwarzalne środowisko i kod
 - docker
 - budowanie swoich obrazów dockera na gitlab: https://medium.com/devops-with-valentine/how-to-build-a-docker-image-and-push-it-to-the-gitlab-container-registry-from-a-gitlab-ci-pipeline-acac0d1f26df
 - python - requirements
 - python - virtualenv https://aaronlelevier.github.io/virtualenv-cheatsheet/
 - python - poetry https://mathspp.com/blog/how-to-create-a-python-package-in-2022
 - python - pyenv https://github.com/pyenv/pyenv

 ## Poetry pyta o hasło i nie działa

 Poetry może używać zapisanych haseł, dla prostszej publikacji do pypi, lub ściągania zależności z prywatnych repozytoriów pythonowych. 

 Jeżeli prosi o hasło i nie działa, to można użyć komendy:

 `export PYTHON_KEYRING_BACKEND=keyring.backends.fail.Keyring`

 dla bieżącego terminalu wyłączy opcję używania zapisanych haseł dla pythona.

 ## nie ma pliku wykonywalnego poetry!

 należy do pliku `~/.bashrc` dodać odpowiednią linijkę, którą podpowiada instalator poetry. Jest to linijka typu:

`export PATH="/dmj/2010/md316445/.local/bin:$PATH"`

Jeżeli pliku nie ma można go stworzyć dowolnym edytorem tekstowym, na przykład nano:

`nano ~/.bashrc`


 ## Pisanie commitu piszczy i niszczy tekst (jak zmienić edytor tekstu używany przez git)
 Zmiana edytora tekstu używanego przez git:

 https://www.toolsqa.com/git/set-up-notepad-for-git-bash/

 tl;dr prostsze edytory niż standardowy vim, wykonaj jedne z poniższych poleceń:

 `git config --global core.editor nano`
 
 `git config --global core.editor notepad.exe`

## Regularne release'y:
 - paczkowanie pythona - wheel, setup.py, setuptools
   - https://mathspp.com/blog/how-to-create-a-python-package-in-2022
   - https://setuptools.pypa.io/en/latest/setuptools.html
   - https://github.com/python-versioneer/python-versioneer#quick-install
   - przykładowy minimalny szkielet projektu pythonowego https://gitlab.com/programowanie-zespolowe-2023/przykladowy-projekt-python
   - jego "pages" https://programowanie-zespolowe-2023.gitlab.io/przykladowy-projekt-python/
 - budowanie applikacji - pyinstaller
 - izolowanie projektu pythonowego i jego zależności:
   - https://aaronlelevier.github.io/virtualenv-cheatsheet/
   - instalowanie swojej wersji python w katalogu domowym:
     - https://github.com/pyenv/pyenv-installer#installation--update--uninstallation
     - https://github.com/pyenv/pyenv/wiki#suggested-build-environment


# Projekty i grupy


- G1 https://gitlab.com/groups/projekt-gra-pijk/-/group_members - Bilard
  - https://gitlab.com/projekt-gra-pijk/bilard/-/boards/5457535
- G2 https://gitlab.com/groups/cyborgen/-/group_members - Sumo Bot
  - https://gitlab.com/cyborgen/robot/-/boards
- G3 https://gitlab.com/groups/programowanie_zespoloweg3/-/group_members - ???
  - https://gitlab.com/programowanie_zespoloweg3/projekt/-/boards
- G4 https://gitlab.com/groups/programowanie_zespolowe3/-/group_members - birdclef
  - https://gitlab.com/programowanie_zespolowe3/Programowanie_zespolowe/-/boards/5457536

Proponowane projekty:


## Symulator Kryształów

Symulacja wzrostu pseudo kryształów:
  - edytor cząstek - cząstek składających się z kilku kulek, z różnymi ładunkami, które mogą się obracać
   - wielkość kulek i moc ładunków można zmieniać
  - symulator fizyki - rzucamy cząstkami do pewnego zamkniętego pomieszczenia
   - cząstki mają pewną masę i moment, mogą kollidować i się odbijać (kolizje elastyczne)
  - wizualizujemy wynik
  - możliwość ładnego renderu wyniku (eksport cząstek do obj, fbx lub podobne)
  - zbudować różne kryształy z różną proporcją wielkości kulek, ładunków, ilości kulek
  - dodatkowe utrudnienie - modelowanie cząstek składających się z kilku kulekm, z różnymi ładunkami, które mogą się obracać
  - podpowiedź: metoda Eulera, Leapfrog symulacji fizyki cząstek


## Symulator życia i ewolucji

Stworzyć system gdzie są symulowane agenci, które:
- muszą mieć jakieś środki lokomocji
- muszą mieć zmysły
- mogą umrzeć z glodu, starości lub uszkodzeń
- mogą się rozmnażać i zmieniać swoje właściwości na podstawie właściwości rodziców oraz losowych mutacji
- powinna istnieć wizualizacja planszy oraz parametrów
- najlepiej aby środowisko było najbardziej fizyczne

 można się natchnąć https://swimbots.com/

 ## Gra w Biliard

 Symulator bilardu, np Ósemki, z trybem gry przez sieć i wyszukiwania opponentów. Np. gry 8-ball, z obliczaniem ELO graczy.
 
 https://en.wikipedia.org/wiki/Elo_rating_system

https://pl.wikipedia.org/wiki/%C3%93semka_(bilard)

 Uwzględnić jak najwięcej "fizyki" ile się da.

## Syntezator muzyczny

- przetważanie plików midi do dźwięku
- parametryczne syntezowanie instrumentów muzycznych, na przykład za pomocą FM-syntesis
- effekty dźwiękowe typu filtrów, echa, chorus, overdrive
- gui

Dowolny inny większy projekt programistyczny zaproponowany przez studentów. W dowolnych technologiach, dowolnej tematyki, gra, program, strona internetowa.




# Zasady zaliczania

Zakres, głębie i detale projektu ustalają grupy. Można w czasie develepowania projekty pogłębiać i polepszać. Symulujemy, ze prowadzący zajęcia jest klientem i może niespodziewanie zarządać dodatkowych cech (Oczywiście też pomoże z ich realizacją, przy trudnościach).


Projekty powinny być prowadzone wspólnie, powinne być robione code review, planowane konkretne opisane zadania, management zadań, podpinanie commitów pod zadania, tak, aby projekt, miał swoją poprawną historę. Projekt ma się testować (ile się sensownie da) i budować automatycznie z każdym commitem, aby można było go pobrać i użyć. Symulujemy działanie firmy programistycznej. Oceniany będzie stan projektu na gitlab (lub analogicznej platformie), jego historia, commity, opisanie tasków it + prezentacja projektu na koniec. Projekt nie musi być idealnie działający, ale musi być tworzony wspólnie i odpowiednio dokumentowany.

Końcowa ocena:
Prowadzenie projektu: 75% oceny
oceniana jest poprawność używania instrumentów do wspólnego programowania:

  - dzielenie się zadaniami
  - spisywanie potrzeb w zadaniach
  - ustalanie standardów projektu
  - regularne wykonywanie wzajemnego przeglądu kodu
  - ciągła integracja (continuous integration)
  - ciągłe dostarczanie (continuous delivery)
  - ciągłe testowanie (continuous testing)

końcowa prezentacja projektu: 25% oceny
    - prezentacja pomysłu, przebiegu implementacji i działającego projektu

W skrócie - jeśli jest działający projekt, prowadzony aby jak - 3. Jeżeli projekt jest prowadzony w grupie z ogólnie przyjętymi zasadamio - 4-5, ale może i nie działać dobrze. Projekt jest prowadzony dobrze i działa 5+.



# Projekty z lat poprzednich, które można spróbować stworzyć lepiej


## CosmoSim
Symulator układu słonecznego w 3D
  - tworzenie Newtonowskiej fizyki
  - wizualizacja (planety, orbity) (dowolne)
  - przewidywanie zaćmień
  - symulator statku kosmicznego 
  - podpowiedź: metoda Eulera, Leapfrog symulacji fizyki


## Motion Capture
Nagrywacz Motion Capture:
   - Warto użyć MediaPipe z kamerką
   - Dla chcących można kinect (będzie trudno)
   - wizualizator nagranego szkieletu
   - filtry (odszumnianie pozycji kości)
   - generacja animacji z filmiku lub na żywo
   - export to formatu animacji Unity, standardowego szkieletu człowieczego
    - pliki animacji to są tekstowe yaml, z pozycjami
    - można podmieniać animowane modele 
    - można wygenerować plik w Unity, po zaimportowaniu odpowiedniego humanoidalnego modelu np:
      - https://assetstore.unity.com/packages/3d/characters/robots/space-robot-kyle-4696
      - model z projektu https://github.com/vrm-c/UniVRM/releases

# AI Narrator
Sztuczny prezenter wiadomości:
   - applikacja/skrypt która pobiera tekst nowiny (opcjonalnie ze linku na stronę z nowinami)
   - generuje narrację głosową (Iwona, 15.ai, google speech etc)
   - tło muzyczne
   - generuje video, z animowanym narratorem (można zaczać od najprostszej twarzy)
   - dodatkowo pokazuje jakiejś obrazki z internetu, pasujące do tekstu (wyszukiwanie słów kluczowych)
   - narrator i obrazki mogą być wybierane oceniając sentyment tekstu np
     - https://realpython.com/python-nltk-sentiment-analysis/
   - opcjonalnie - gałąź na git, której commit ustawia tekst a wynikiem CI/CD jest video wżucone na youtube




Dowolny inny większy projekt zaproponowany przez studentów.







